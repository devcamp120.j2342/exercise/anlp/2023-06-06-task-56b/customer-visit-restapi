package com.devcamp.customervisitrestapi.models;

import java.util.Date;

public class Visit {
    private Customer customer;
    private Date date;
    private double serviceExpense;
    private double productExpense;

    public Visit(String name, Date date) {
        this.customer = new Customer(name);
        this.date = date;
    }


    public Visit(String name, Date date, double serviceExpense, double productExpense) {
        this.customer = new Customer(name);
        this.date = date;
        this.serviceExpense = serviceExpense;
        this.productExpense = productExpense;
    }


    public Customer getCustomer() {
        return this.customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getName() {
        return this.customer.getName();
    }

    public double getServiceExpense() {
        return this.serviceExpense;
    }

    public void setServiceExpense(double serviceExpense) {
        this.serviceExpense = serviceExpense;
    }

    public double getProductExpense() {
        return this.productExpense;
    }

    public void setProductExpense(double productExpense) {
        this.productExpense = productExpense;
    }

    public double getTotalExpens() {
        return this.serviceExpense + this.productExpense;
    }

    @Override
    public String toString() {
        return "Visit[" +
            getName() +
            ", serviceExpense='" + getServiceExpense() + "'" +
            ", productExpense='" + getProductExpense() + "'" +
            ", totalExpense='" + getTotalExpens() + "'" +
            "]";
    }
}
