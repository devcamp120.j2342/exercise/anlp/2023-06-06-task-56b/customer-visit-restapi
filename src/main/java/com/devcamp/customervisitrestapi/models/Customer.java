package com.devcamp.customervisitrestapi.models;

public class Customer {
    private String name;
    private boolean member =false;
    private String memberType;

    public Customer(String name) {
        this.name = name;
    }

    public Customer(String name, String memberType) {
        this.name = name;
        this.member = true;
        this.memberType = memberType;
    }

    public String getName() {
        return this.name;
    }

    public boolean isMember() {
        return this.member;
    }

    public void setMember(boolean member) {
        this.member = member;
    }

    public String getMemberType() {
        return this.memberType;
    }

    public void setMemberType(String memberType) {
        this.memberType = memberType;
    }

    @Override
    public String toString() {
        return "Customer[" +
            " name='" + getName() + "'" +
            ", member='" + isMember() + "'" +
            ", memberType='" + getMemberType() + "'" +
            "]";
    }
}
