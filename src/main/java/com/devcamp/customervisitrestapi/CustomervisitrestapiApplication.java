package com.devcamp.customervisitrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomervisitrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomervisitrestapiApplication.class, args);
	}

}
