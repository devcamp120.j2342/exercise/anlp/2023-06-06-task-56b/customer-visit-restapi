package com.devcamp.customervisitrestapi.controllers;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitrestapi.models.Customer;
import com.devcamp.customervisitrestapi.models.Visit;

@RestController
@RequestMapping("/")
@CrossOrigin
public class CustomerVisitAPIcontroller {
    @GetMapping("/visits")
    public ArrayList<Visit> getVisits(){
        Customer customer1 = new Customer("an");
        Customer customer2 = new Customer("minh","gold");
        Customer customer3 = new Customer("hieu","gold");

        System.out.println(customer1);
        System.out.println(customer2);
        System.out.println(customer3); 



        Date date = new Date();
        Visit visit1 = new Visit("an",date,10000,50000);
        Visit visit2 = new Visit("minh",date,50000,200000);
        Visit visit3 = new Visit("hieu",date,50000,25000);

        System.out.println(visit1);
        System.out.println(visit2);
        System.out.println(visit3);

        
        ArrayList<Visit> arrListVisit = new ArrayList<Visit>();

        arrListVisit.add(visit1);
        arrListVisit.add(visit2);
        arrListVisit.add(visit3);

        return arrListVisit;

    }
}
